package main

import (
	"log"

	"gitlab.com/koalalorenzo/example-gmdbtp/examplepkg"
)

func main() {
	log.Print("Hello", examplepkg.ReturnAwesomeString())
}
