package examplepkg

const (
	// AmazingString is just amazing
	AmazingString = "World"
)

// ReturnAwesomeString will return the amazing string
func ReturnAwesomeString() string {
	return AmazingString
}
