package examplepkg

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestStrings(t *testing.T) {
	assert.Equal(t, "World", ReturnAwesomeString())
}
