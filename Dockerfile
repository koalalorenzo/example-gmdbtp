FROM golang:1.8-alpine as builder
MAINTAINER Lorenzo Setale <lorenzo.setale@siderus.io>

# Install some deps
RUN apk --no-cache add bash git gcc musl-dev

WORKDIR /go/src/gitlab.com/koalalorenzo/example-gmdbtp/
COPY . .
RUN \
  go get github.com/stretchr/testify; \
  go build -o runme ;\
  go test -c ./examplepkg -o examplepkg.test

# You can also use scratch or alpine
FROM alpine
COPY --from=builder /go/src/gitlab.com/koalalorenzo/example-gmdbtp/runme /runme
COPY --from=builder /go/src/gitlab.com/koalalorenzo/example-gmdbtp/examplepkg.test /examplepkg.test

CMD ["/runme"]
